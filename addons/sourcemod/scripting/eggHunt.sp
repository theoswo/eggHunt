#pragma semicolon 1

#define PLUGIN_VERSION "1.1"
#define PL_P "[\x06EggSearch\x01]"

#include <sourcemod>
#include <adminmenu>
#include <sdkhooks>
#include <sdktools>
#include <cstrike>

#include "eggHunt/menus.sp"
#include "eggHunt/team.sp"
#include "eggHunt/misc.sp"
#include "eggHunt/database.sp"
#include "eggHunt/eggs.sp"
#include "eggHunt/commands.sp"

public Plugin myinfo = 
{
	name = "Easter Egg Search",
	author = "Oscar Wos (OSWO)",
	description = "Gamemode Plugin",
	version = PLUGIN_VERSION,
	url = "www.tangoworldwide.net",
};

public void OnPluginStart()
{	
	RegConsoleCmd("sm_easter", command_easter);
	RegConsoleCmd("jointeam", command_joinTeam);
	
	HookEvent("player_connect", event_playerConnect, EventHookMode_Pre);
	HookEvent("player_team", event_playerTeam, EventHookMode_Pre);
	HookEvent("round_start", event_roundStart, EventHookMode_Post);
	HookEvent("round_end", event_roundEnd, EventHookMode_Pre);
	
	for (new client = 1; client <= MaxClients; client++) 
	{ 
		if (IsClientInGame(client))
			SDKHook(client, SDKHook_OnTakeDamage, TakeDamageHook);
	}
    
	ConnectToSQL();
}

public void OnMapStart()
{
	PrecacheStuff();
}

public void OnClientPostAdminCheck(client)
{
	char clientName[MAX_NAME_LENGTH];
	GetClientName(client, clientName, sizeof(clientName));
	
	PrintToChatAll("Welcome %s to the server!", clientName);
	
	SDKHook(client, SDKHook_OnTakeDamage, TakeDamageHook);
	
	DB_PConnect(client);
}

public void OnClientDisconnect(client)
{
	DB_PDisconnect(client);
}