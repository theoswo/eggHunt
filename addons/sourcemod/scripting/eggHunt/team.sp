public EasterChangeTeam(int client, int team)
{
	switch (team)
	{
		case 0:
		{
			CS_SwitchTeam(client, 0);
		}
		
		case 1:
		{
			CS_SwitchTeam(client, 0);
		}
			
		case 2:
		{
			CS_SwitchTeam(client, 2);
		}
			
		case 3:
		{
			CS_SwitchTeam(client, 3);
			SetEntityModel(client, "models/tango/bunny/bunny_funny.mdl");
		}
			
		default:
	    	PrintToChat(client, "%s You specified an invalid team number!", PL_P);
    }
}

public ForceTeamChange(int client, int target, int team)
{
	char targetName[MAX_NAME_LENGTH];
	GetClientName(target, targetName, sizeof(targetName));
	
	if (!CanUserTarget(client, target))
		PrintToChat(client, "%s You cannot target the player: \x07%s", PL_P, targetName); 
	else 
	{
		if (GetClientTeam(target) != team)
		{	
    		char targetTeamName[8];
    		
    		if (team == 3)
    			Format(targetTeamName, sizeof(targetTeamName), "CT");
    		else if (team == 2)
    			Format(targetTeamName, sizeof(targetTeamName), "T");
    		else
    			Format(targetTeamName, sizeof(targetTeamName), "Spec");
    			
			ShowActivity2(client, "%s \x0D", "\x01Moved %s to \x07%s", PL_P, targetName, targetTeamName);
			EasterChangeTeam(target, team);
		} 
		else
			PrintToChat(client, "%s Target is already on that team!", PL_P);		
	}
}