Handle DB = INVALID_HANDLE;

public ConnectToSQL()
{
	char errormsg[512];
	DB = SQL_Connect("easter", true, errormsg, sizeof(errormsg));
	
	if(DB == INVALID_HANDLE)
	{
		PrintToServer("Cannot connect to MySQL, Error: %s", errormsg);
		CloseHandle(DB);
	}
	MakeTables();
}

public void MakeTables()
{
	char buffer[512];
	Handle sendQuery;
	
	Format(buffer, sizeof(buffer), "CREATE TABLE IF NOT EXISTS `ea_Players` (`SteamID` varchar(32) NOT NULL, `LastName` varchar(128) NOT NULL, `LastConnect` varchar(128) NOT NULL, `FirstConnect` varchar(128) NOT NULL, `Connections` int NOT NULL, PRIMARY KEY (`SteamID`));");
	sendQuery = SQL_Query(DB, buffer);
	
	Format(buffer, sizeof(buffer), "CREATE TABLE IF NOT EXISTS `ea_Tracker` (`SteamID` varchar(32) NOT NULL, `BrokenStuff` int NOT NULL, `RandPickedCT` int NOT NULL, PRIMARY KEY (`SteamID`));");
	sendQuery = SQL_Query(DB, buffer);
	
	CloseHandle(sendQuery);
}

public void DB_PConnect(int client)
{
	char buffer[512];
	Handle sendQuery;
		
	char clientSteamId[128], theTime[128], clientName[MAX_NAME_LENGTH];
	
	GetClientAuthId(client, AuthId_Engine, clientSteamId, sizeof(clientSteamId));
	FormatTime(theTime, sizeof(theTime), "%d %B %G %H:%M:%S");
	GetClientName(client, clientName, sizeof(clientName));
		
	Format(buffer, sizeof(buffer), "INSERT INTO `ea_Players` (SteamID, LastName, LastConnect, FirstConnect, Connections) VALUES ('%s', '0', '0', '%s', '0')", clientSteamId, theTime);
	sendQuery = SQL_Query(DB, buffer);
			
	Format(buffer, sizeof(buffer), "INSERT INTO `ea_Tracker` (SteamID, BrokenStuff, RandPickedCT) VALUES ('%s', '0', '0')", clientSteamId);
	sendQuery = SQL_Query(DB, buffer);
		
	Format(buffer, sizeof(buffer), "UPDATE `ea_Players` SET `LastName` = '%s', `LastConnect` = 'Now Playing', `Connections` = `Connections` + 1 WHERE `SteamID` = '%s'", clientName, clientSteamId);
	sendQuery = SQL_Query(DB, buffer);
		
	CloseHandle(sendQuery);
}

public void DB_PDisconnect(int client)
{
	char buffer[512];
	Handle sendQuery;
		
	char clientSteamId[128], theTime[128], clientName[MAX_NAME_LENGTH];
	
	GetClientAuthId(client, AuthId_Engine, clientSteamId, sizeof(clientSteamId));
	FormatTime(theTime, sizeof(theTime), "%d %B %G %H:%M:%S");
	GetClientName(client, clientName, sizeof(clientName));
		
	Format(buffer, sizeof(buffer), "UPDATE `ea_Players` SET `LastName` = '%s', `LastConnect` = '%s' WHERE `SteamID` = '%s'", clientName, theTime, clientSteamId);
	sendQuery = SQL_Query(DB, buffer);
		
	CloseHandle(sendQuery);
}

//Fix This?
public void DB_FixConnections()
{
	for (int i = 1; i < MaxClients; i++)
	{
		if (IsValidClient(i))
		{
			char buffer[512];
			Handle sendQuery;
			
			char clientSteamId[128];
			GetClientAuthId(i, AuthId_Engine, clientSteamId, sizeof(clientSteamId));
			Format(buffer, sizeof(buffer), "UPDATE `ea_Players` SET `Connections` = `Connections` - 1 WHERE `SteamID` = '%s'", clientSteamId);
			sendQuery = SQL_Query(DB, buffer);
			
			CloseHandle(sendQuery);
		}
	}
}