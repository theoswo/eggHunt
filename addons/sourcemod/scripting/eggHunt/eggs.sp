Handle g_HintText[MAXPLAYERS + 1];
Handle g_PreRoundInProgressTimer;
Handle g_RoundInProgressTimer;
Handle g_BunnyRefreshTimer = INVALID_HANDLE;

bool g_RoundStart;
bool g_PreRoundInProgress;
bool g_RoundInProgress;
bool g_RoundEnd;
bool g_FailedBunny;

float g_TimeOut;

int g_BunnyId;

int g_EggCount;
int g_RemainingEggs;
//char g_MostFoundName[512];

public void ShowEggHintText(client)
{
	if (IsValidClient(client) && IsClientInGame(client))
		g_HintText[client] = CreateTimer(0.1, refreshHintText, client, TIMER_REPEAT);
	PrintToChat(client, "%s Gamemode made by: Oscar Wos!", PL_P);
}

public Action refreshHintText(Handle timer, int client)
{
	if (!IsClientInGame(client))
	{
		CloseHandle(g_HintText[client]);
		g_HintText[client] = INVALID_HANDLE;
	}
	
	char buffer[512];
	Format(buffer, sizeof(buffer), "<font size='20' color='#00FF00'>Egg Hunt</font> \n");
	
	if (!g_RoundEnd)
	{
		if (!(IsPlayerAlive(client)))
		{
			Format(buffer, sizeof(buffer), "%s <font size='16'>You'll be able to play next game! \n", buffer);
			
			if (g_RoundStart)
				Format(buffer, sizeof(buffer), "%s New Game is starting soon!\n", buffer);
			else if (g_PreRoundInProgress)
				Format(buffer, sizeof(buffer), "%s <font color='#0000FF'>Bunny is hiding eggs!\n", buffer);
			else if (g_RoundInProgress)
				Format(buffer, sizeof(buffer), "%s <font color='#FFFF00'>Eggs Left To Find: %i", buffer, g_EggCount);
		} 
		else 
		{
			if (g_RoundStart)
			{
				Format(buffer, sizeof(buffer), "%s <font size='16'>Welcome to Easter Egg Search!\n", buffer);
				Format(buffer, sizeof(buffer), "%s The Game will start briefly!", buffer);
			}
			else if (g_PreRoundInProgress)
			{
				if (GetClientTeam(client) == 3)
				{
					Format(buffer, sizeof(buffer), "%s <font size='16'>You're the <font color='#0000FF'>Bunny!</font>\n", buffer);
					Format(buffer, sizeof(buffer), "%s <font size='16'>You should hide all the eggs!\n", buffer);
				} else {	
					Format(buffer, sizeof(buffer), "%s <font size='16'>The Bunny is hiding the eggs!\n", buffer);
					Format(buffer, sizeof(buffer), "%s Wait 1 minute, or until all eggs are placed!", buffer);
				}
			}
			else if (g_RoundInProgress)
			{
				if (GetClientTeam(client) == 3)
				{
					Format(buffer, sizeof(buffer), "%s <font size='16'>Use !easter to use your EasterCredits!\n", buffer);
					Format(buffer, sizeof(buffer), "%s <font color='#FF0000'>Eggs Left to Find: %i, buffer, g_EggCount);
				} else {
					Format(buffer, sizeof(buffer), "%s <font size='16'>Use !easter to use your EasterCredits!\n", buffer);
					Format(buffer, sizeof(buffer), "%s <font color='#FFFF00'>Eggs Left To Find: %i", buffer, g_EggCount);
				}
			}
		}
	} 
	else 
	{
		Format(buffer, sizeof(buffer), "%s <font size='16'>Thanks for Playing this round!\n", buffer);
		//Add Most Eggs Found
		Format(buffer, sizeof(buffer), "%s Most Eggs Found: \n", buffer);
	}
	
	PrintHintText(client, buffer);
}

public Action event_roundStart(Event event, char[] name, bool dontBroadcast)
{
	g_RoundStart = true;
	g_RoundEnd = false;
	g_PreRoundInProgressTimer = CreateTimer(15.0, preRoundInProgress);
	
	for (int i = 1; i < MaxClients; i++)
	{
		if (IsValidClient(i) && IsClientInGame(i))
			ShowEggHintText(i);
	}
}

public Action preRoundInProgress(Handle timer)
{
	g_RoundStart = false;
	g_PreRoundInProgress = true;
	
	/*
	int TotalNumPlayers;
	
	for (int i = 1; i < MaxClients; i++)
	{
		if (IsValidClient(i))
			TotalNumPlayers++;
	}
	
	TotalNumPlayers = TotalNumPlayers / 3;
	
	if (TotalNumPlayers <= 1)
		PickRandomPlayer();
	else if (TotalNumPlayers > 1 && TotalNumPlayers <= 2)
	{
		for (int i = 1; i < 2; i++)
			PickRandomPlayer();
	} else if (TotalNumPlayers > 2 && TotalNumPlayers <= 3) {
		for (int i = 1; i < 3; i++)
			PickRandomPlayer();
	}
	*/
	
	PickRandomPlayer();
	
	g_RoundInProgressTimer = CreateTimer(60.0, roundInProgress);
}

public Action roundInProgress(Handle timer)
{
	g_PreRoundInProgress = false;
	g_RoundInProgress = true;
}

public Action event_roundEnd(Event event, char[] name, bool dontBroadcast)
{
	ForceRoundEnd();
}

public void ForceRoundEnd()
{
	for (int i = 1; i < MaxClients; i++)
	{
		if (IsValidClient(i))
		{
			EasterChangeTeam(i, 2);
		}
	}
	
	if (g_BunnyRefreshTimer != INVALID_HANDLE)
	{
		CloseHandle(g_BunnyRefreshTimer);
		g_BunnyRefreshTimer = INVALID_HANDLE;
	}
	
	if (g_RoundStart)
		CloseHandle(g_PreRoundInProgressTimer);
	else if (g_PreRoundInProgress)
		CloseHandle(g_RoundInProgressTimer);
	
	g_RoundStart = false;
	g_PreRoundInProgress = false;
	g_RoundInProgress = false;
	
	g_RoundEnd = true;
	
	g_TimeOut = 0.0;
	g_EggCount = 0;
	g_RemainingEggs = 0;
	g_FailedBunny = false;
	
	for (int i = 1; i < MaxClients; i++)
	{
		if (IsValidClient(i))
			ForcePlayerSuicide(i);
	}
	PrintToChatAll("%s Everyone's been moved to \x07T!", PL_P);
}

stock PickRandomPlayer()
{
	int clients[MAXPLAYERS + 1];
	int clientCount;
	int NewCTIndex;
	char NewCTIndexName[128];
	
	for (int i = 1; i < MaxClients; i++)
	{
		if (IsValidClient(i))
			clients[clientCount++] = i;
	}
	
	NewCTIndex = clients[GetRandomInt(0, clientCount - 1)];
	EasterChangeTeam(NewCTIndex, 3);
	GetClientName(NewCTIndex, NewCTIndexName, sizeof(NewCTIndexName));
	
	int temp = GetClientCount();
	
	if (temp <= 5)
		g_RemainingEggs = 10;
	else
		g_RemainingEggs = temp * 2;
		
	g_BunnyRefreshTimer = CreateTimer(0.1, refreshBunnyMenu, NewCTIndex, TIMER_REPEAT);
	
	g_TimeOut = 60.0;
	
	g_BunnyId = NewCTIndex;
	PrintToChatAll("%s %s Is now the \x0BBunny!", PL_P, NewCTIndexName);
}

public Action refreshBunnyMenu(Handle timer, int client)
{
	ShowBunnyMenu(client);
	
	g_TimeOut = g_TimeOut - 0.1;
	
	if (g_TimeOut <= 0.0 && g_EggCount == 0)
	{
		g_FailedBunny = true;
		CreateTimer(0.3, failedBunnyRefresh);
	} 
	else if (g_TimeOut <= 0.0 && g_EggCount != 0) 
	{
		CloseHandle(g_BunnyRefreshTimer);
		g_BunnyRefreshTimer = INVALID_HANDLE;
	}
}

public Action failedBunnyRefresh(Handle timer)
{
	ForceRoundEnd();
	CreateTimer(3.0, refreshMenu);
}

public Action refreshMenu(Handle timer)
{
	Menu menu = new Menu(RefreshHandle);
	menu.Display(g_BunnyId, 5);
}

public int RefreshHandle(Menu menu, MenuAction action, int client, int option)
{
}

/*Bunny Menu*/
public ShowBunnyMenu(int client)
{
	char buffer[512];
	
	Menu menu = new Menu(EasterBunnyHandle);
	if (!g_FailedBunny)
	{
		Format(buffer, sizeof(buffer), "Bunny Menu - Time Left %.1f", g_TimeOut);
		menu.SetTitle(buffer);
		
		Format(buffer, sizeof(buffer), "Total Eggs Remaining: %i", g_RemainingEggs);
		menu.AddItem(buffer, buffer, ITEMDRAW_DISABLED);
		if (g_RemainingEggs > 0)
		{
			Format(buffer, sizeof(buffer), "Place Egg - Total Placed: %i", g_EggCount);
			menu.AddItem(buffer, buffer);
		} else {
			Format(buffer, sizeof(buffer), "Cannot Place More Eggs! - Total Placed: %i", g_EggCount);
			menu.AddItem(buffer, buffer, ITEMDRAW_DISABLED);
		}
	} else {
		Format(buffer, sizeof(buffer), "Bunny Menu - You Failed to place any Eggs!");
		menu.SetTitle(buffer);
		
		Format(buffer, sizeof(buffer), "You Failed!");
		menu.AddItem(buffer, buffer, ITEMDRAW_DISABLED);
		menu.AddItem(buffer, buffer, ITEMDRAW_DISABLED);
		menu.AddItem(buffer, buffer, ITEMDRAW_DISABLED);
	}
	
	menu.ExitButton = false;
	menu.Display(client, 60);
}

public int EasterBunnyHandle(Menu menu, MenuAction action, int client, int option)
{
	if (action == MenuAction_Select)
	{
		if (!g_FailedBunny)
		{
			switch (option)
			{
				case 1:
					PlaceEgg();
			}
		}
	}
}

public PlaceEgg()
{
	g_EggCount++;
	g_RemainingEggs--;
}