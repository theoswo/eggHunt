public Action TakeDamageHook(client, &attacker, &inflictor, &Float:damage, &damagetype)
{
    if ( (client>=1) && (client<=MaxClients) && (attacker>=1) && (attacker<=MaxClients) && (attacker==inflictor) )
    {
        damage *= 0;
        return Plugin_Handled;
    }
    return Plugin_Continue;
}

public Action event_playerConnect(Event event, char[] name, bool dontBroadcast)
{
	dontBroadcast = true;
	return Plugin_Changed;
}

public Action event_playerTeam(Event event, char[] name, bool dontBroadcast)
{
	dontBroadcast = true;
	return Plugin_Changed;
}

public void PrecacheStuff()
{
	AddFileToDownloadsTable("models/tango/bunny/bunny.vmt");
	AddFileToDownloadsTable("models/tango/bunny/bunny.vtf");
	AddFileToDownloadsTable("models/tango/bunny/bunny_funny.dx80");
	AddFileToDownloadsTable("models/tango/bunny/bunny_funny.dx90");
	AddFileToDownloadsTable("models/tango/bunny/bunny_funny.mdl");
	AddFileToDownloadsTable("models/tango/bunny/bunny_funny.phy");
	AddFileToDownloadsTable("models/tango/bunny/bunny_funny.sw");
	AddFileToDownloadsTable("models/tango/bunny/bunny_funny.vvd");
	AddFileToDownloadsTable("models/tango/bunny/bunny_funny.vtf");
	
	PrecacheModel("models/tango/bunny/bunny_funny.mdl");
}

stock bool IsValidClient(int client)
{
	if (client >= 1 && client <= MaxClients && IsValidEntity(client) && IsClientConnected(client) && IsClientInGame(client))
		return true;
	return false;
}