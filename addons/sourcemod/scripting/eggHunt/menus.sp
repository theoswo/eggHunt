/*Show Easter Menu */
public ShowEasterMenu(int client)
{
	Menu menu = new Menu(EasterMenuHandle);
	char buffer[512];
	
	Format(buffer, sizeof(buffer), "Egg Hunt Menu!");
	menu.SetTitle(buffer);
	
	AdminId clientAdmin = GetUserAdmin(client);
	
	if (clientAdmin != INVALID_ADMIN_ID && GetAdminFlag(clientAdmin, Admin_Ban, Access_Effective))
	{
		Format(buffer, sizeof(buffer), "Admin Menu");
		menu.AddItem(buffer, buffer);
	} else {
		Format(buffer, sizeof(buffer), "Admin Menu - Disabled");
		menu.AddItem(buffer, buffer, ITEMDRAW_DISABLED);
	}
	
	if (clientAdmin != INVALID_ADMIN_ID && GetAdminFlag(clientAdmin, Admin_Custom6, Access_Effective))
	{
		Format(buffer, sizeof(buffer), "Vip Menu");
		menu.AddItem(buffer, buffer);
	} else {
		Format(buffer, sizeof(buffer), "Vip Menu - Disabled");
		menu.AddItem(buffer, buffer);
	}
	
	menu.Display(client, 0);
}
public int EasterMenuHandle(Menu menu, MenuAction action, int client, int option)
{
	if (action == MenuAction_Select)
	{
		switch (option)
		{
			case 0:
				ShowAdminEasterMenu(client);
			case 1: {}
				//ShowVipEasterMenu(client);
		}
	}
}

/*Show Easter Admin Menu*/
public ShowAdminEasterMenu(int client)
{
	Menu menu = new Menu(EasterAdminMenuHandle);
	char buffer[512];
	
	Format(buffer, sizeof(buffer), "Egg Hunt Menu - Admin");
	menu.SetTitle(buffer);
	
	Format(buffer, sizeof(buffer), "Force move a Player");
	menu.AddItem(buffer, buffer);
	
	menu.Display(client, 0);
}
public int EasterAdminMenuHandle(Menu menu, MenuAction action, int client, int option)
{
	if (action == MenuAction_Select)
	{
		switch (option)
		{
			case 0:
			{
				MovePlayerMenu(client);
			}
		}
	}
	
	else if (action == MenuAction_Cancel) 
		ShowEasterMenu(client);
}
public MovePlayerMenu(client)
{
	char buffer[512];
	Menu menu = new Menu(EasterAdminMoveMenuHandle);
	
	Format(buffer, sizeof(buffer), "Egg Hunt Menu - Admin - Force Move");
	menu.SetTitle(buffer);
	
	Format(buffer, sizeof(buffer), "CT - Bunny");
	menu.AddItem(buffer, buffer);
	
	Format(buffer, sizeof(buffer), "T - Seekers");
	menu.AddItem(buffer, buffer);
	
	menu.Display(client, 0);
}

public int EasterAdminMoveMenuHandle(Menu menu, MenuAction action, int client, int option)
{
	if (action == MenuAction_Select)
	{
		switch (option)
		{
			case 0:
				DrawMovePlayerMenu(client, 3);
			case 1:
				DrawMovePlayerMenu(client, 2);
		}
	}
}

/*Draw Move Player Menu*/
public DrawMovePlayerMenu(int client, int team)
{
	char buffer[512];
	
	Menu menu = new Menu(EasterAdminPlayerHandle);
	menu.SetTitle("Choose a Player to Switch");
	
	for (int i = 1; i < MaxClients; i++)
	{
		if (IsValidClient(i))
		{
			char clientName[MAX_NAME_LENGTH];
			GetClientName(i, clientName, sizeof(clientName));
			
			DataPack datapack = new DataPack();
			datapack.WriteCell(i);
			datapack.WriteCell(team);
			
			Format(buffer, sizeof(buffer), "%d", datapack);
			
			menu.AddItem(buffer, clientName);
		}
	}
	
	menu.Display(client, 0);
}
public int EasterAdminPlayerHandle(Menu menu, MenuAction action, int client, int option)
{
	if (action == MenuAction_Select)
	{	
		//Extracting the DataPack to be used. Thanks to: https://forums.alliedmods.net/showthread.php?t=197989
		char optionInfo[128];
		menu.GetItem(option, optionInfo, sizeof(optionInfo));
		new Handle:datapack = Handle:StringToInt(optionInfo);
		ResetPack(datapack);
		
		int targetIndex = ReadPackCell(datapack);
		int targetTeam = ReadPackCell(datapack);
		
		ForceTeamChange(client, targetIndex, targetTeam);
	}
}
