public Action command_easter(int client, int args)
{
	ShowEasterMenu(client);
}

public Action command_joinTeam(int client, int args)
{
	char clientArg[8];
	GetCmdArg(1, clientArg, sizeof(clientArg));
	int clientTeam = StringToInt(clientArg);
	
	if (GetClientTeam(client) == clientTeam)
		PrintToChat(client, "%s You're already on that team!", PL_P);
	else {
		if (clientTeam == 3)
		{
			PrintToChat(client, "%s You've been blocked from switching to \x07CT!", PL_P);
		} else
			EasterChangeTeam(client, clientTeam);
	}

	return Plugin_Handled;
}